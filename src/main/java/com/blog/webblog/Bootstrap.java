package com.blog.webblog;

import com.blog.webblog.entity.Category;
import com.blog.webblog.entity.Param;
import com.blog.webblog.entity.Suggestion;
import com.blog.webblog.entity.User;
import com.blog.webblog.repository.CategoryRepository;
import com.blog.webblog.repository.ParamRepository;
import com.blog.webblog.repository.UserRepository;
import com.blog.webblog.service.ParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ParamRepository paramRepository;

    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setEmail("nazif@hotmail.com");
        user.setPassword("123");
        user.setName("Nazif Emre");
        user.setSurname("Dayı");
        userRepository.save(user);

        User user2 = new User();
        user2.setEmail("user@hotmail.com");
        user2.setPassword("123");
        user2.setName("username");
        user2.setSurname("usersurname");
        userRepository.save(user2);

        Category category = new Category();
        category.setName("Yazılım");
        categoryRepository.save(category);

        Category category2 = new Category();
        category2.setName("Eğlence");
        categoryRepository.save(category2);

        Param param = new Param();
        param.setName("PHONE_NUMBER");
        param.setValue("0532 789 14 78");
        paramRepository.save(param);

        Param param2 = new Param();
        param2.setName("MAIL");
        param2.setValue("nazifemre4507@gmail.com");
        paramRepository.save(param2);

        Param param3 = new Param();
        param3.setName("PAGE_DESC");
        param3.setValue("Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique quos totam id et ab! Debitis ad ipsum, recusandae soluta odit quod vel nostrum nulla eaque hic harum repellendus possimus. Aperiam!");
        paramRepository.save(param3);
    }
}
