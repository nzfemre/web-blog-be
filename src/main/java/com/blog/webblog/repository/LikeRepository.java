package com.blog.webblog.repository;

import com.blog.webblog.entity.Like;
import com.blog.webblog.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LikeRepository extends JpaRepository<Like, Post> {
}
