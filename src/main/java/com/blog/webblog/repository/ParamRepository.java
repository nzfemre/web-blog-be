package com.blog.webblog.repository;

import com.blog.webblog.entity.Param;
import com.blog.webblog.entity.Suggestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParamRepository extends JpaRepository<Param, Long> {
}
