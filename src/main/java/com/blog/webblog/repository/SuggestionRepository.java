package com.blog.webblog.repository;

import com.blog.webblog.entity.Category;
import com.blog.webblog.entity.Suggestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuggestionRepository extends JpaRepository<Suggestion, Long> {
}
