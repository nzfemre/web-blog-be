package com.blog.webblog.controller;

import com.blog.webblog.dto.PostDto;
import com.blog.webblog.entity.Like;
import com.blog.webblog.entity.Param;
import com.blog.webblog.entity.Post;
import com.blog.webblog.service.CategoryService;
import com.blog.webblog.service.ParamService;
import com.blog.webblog.service.PostService;
import com.blog.webblog.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/param")
@CrossOrigin
public class ParamController {

    private final ParamService paramService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Param>> getParams() {
        return ResponseEntity.ok(paramService.getParams());
    }
}
