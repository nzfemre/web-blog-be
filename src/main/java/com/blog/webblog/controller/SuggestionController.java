package com.blog.webblog.controller;

import com.blog.webblog.entity.Category;
import com.blog.webblog.entity.Suggestion;
import com.blog.webblog.service.CategoryService;
import com.blog.webblog.service.SuggestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/suggestion")
@CrossOrigin
public class SuggestionController {

    private final SuggestionService suggestionService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public  ResponseEntity<List<Suggestion>> getCategories() {
       return ResponseEntity.ok( suggestionService.getSuggestions());
    }

    @PostMapping
    @ResponseBody
    public void saveCategory(@RequestBody Suggestion suggestion) {
        suggestionService.saveSuggestion(suggestion);
    }
}
