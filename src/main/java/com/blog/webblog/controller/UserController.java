package com.blog.webblog.controller;

import com.blog.webblog.dto.LoginDto;
import com.blog.webblog.entity.User;
import com.blog.webblog.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    private final UserService userService;

    @PostMapping("/login")
    @ResponseBody
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity<User> login(@RequestBody LoginDto loginDto) {
        User user = userService.getUserByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword());
        if (user != null) return ResponseEntity.ok(user);
        return ResponseEntity.badRequest().body(null);
    }
}
