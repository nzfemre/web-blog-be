package com.blog.webblog.controller;

import com.blog.webblog.dto.CommentDto;
import com.blog.webblog.dto.LoginDto;
import com.blog.webblog.entity.Comment;
import com.blog.webblog.entity.Post;
import com.blog.webblog.entity.User;
import com.blog.webblog.service.CommentService;
import com.blog.webblog.service.PostService;
import com.blog.webblog.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/comment")
@CrossOrigin
public class CommentController {

    private final CommentService commentService;
    private final PostService postService;
    private final UserService userService;


    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Comment>> getCommentsByPostId(@PathVariable Long postId) {
        return ResponseEntity.ok(commentService.getCommentsByPostId(postId));
    }

    @PostMapping
    @ResponseBody
    public void saveComment(@RequestBody CommentDto commentDto) {
        Comment comment = new Comment();
        if(commentDto.getId() != null)
            comment.setId(commentDto.getId());
        comment.setContext(commentDto.getContext());
        comment.setPost(postService.getPost(commentDto.getPostId()));
        comment.setUser(userService.getUser(commentDto.getUserId()));
        commentService.saveComment(comment);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteComment(@PathVariable Long id) {
        commentService.deleteComment(id);
    }
}
