package com.blog.webblog.controller;

import com.blog.webblog.dto.LikeDto;
import com.blog.webblog.entity.Like;
import com.blog.webblog.entity.Post;
import com.blog.webblog.service.LikeService;
import com.blog.webblog.service.PostService;
import com.blog.webblog.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/like")
@CrossOrigin
public class LikeController {
    private final LikeService likeService;
    private final PostService postService;
    private final UserService userService;

    @PostMapping
    @ResponseBody
    public void likePost(@RequestBody LikeDto likeDto) {
        Like like = new Like();
        like.setPost(postService.getPost(likeDto.getPostId()));
        like.setUser(userService.getUser(likeDto.getUserId()));
        like.setLikeAction(likeDto.getLikeAction());
        likeService.saveLike(like);
    }
}
