package com.blog.webblog.controller;

import com.blog.webblog.dto.CommentDto;
import com.blog.webblog.entity.Category;
import com.blog.webblog.entity.Comment;
import com.blog.webblog.entity.Post;
import com.blog.webblog.service.CategoryService;
import com.blog.webblog.service.CommentService;
import com.blog.webblog.service.PostService;
import com.blog.webblog.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/category")
@CrossOrigin
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public  ResponseEntity<List<Category>> getCategories() {
       return ResponseEntity.ok( categoryService.getCategories());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCategory(@PathVariable Long id) {
        categoryService.deleteCategory(id);
    }

    @PostMapping
    @ResponseBody
    public void saveCategory(@RequestBody Category category) {
        categoryService.saveCategory(category);
    }
}
