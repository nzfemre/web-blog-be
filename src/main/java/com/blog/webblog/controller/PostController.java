package com.blog.webblog.controller;

import com.blog.webblog.dto.PostDto;
import com.blog.webblog.entity.Like;
import com.blog.webblog.entity.Post;
import com.blog.webblog.service.CategoryService;
import com.blog.webblog.service.PostService;
import com.blog.webblog.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/post")
@CrossOrigin
public class PostController {

    private final PostService postService;
    private final UserService userService;
    private final CategoryService categoryService;
    private final ObjectMapper objectMapper;

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Post>> getPosts() {
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        List<Post> posts = postService.getPosts();
        posts.forEach(p -> p.getLikes().forEach(Like::setUserId));
        return ResponseEntity.ok(posts);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity addPost(@RequestBody PostDto postDto) {
        Post post = new Post();
        if(postDto.getId() != null)
            post.setId(postDto.getId());
        post.setTitle(postDto.getTitle());
        post.setContext(postDto.getContext());
        post.setCategory(categoryService.getCategoryByName(postDto.getCategory()));
        post.setUser(userService.getUser(postDto.getUserId()));
        post.setDate(new Date().toString());
        postService.savePost(post);
        return ResponseEntity.ok().body(null);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deletePost(@PathVariable Long id) {
        postService.deletePost(id);
    }
}
