package com.blog.webblog.service;

import com.blog.webblog.entity.Category;
import com.blog.webblog.entity.Comment;
import com.blog.webblog.entity.User;
import com.blog.webblog.repository.CategoryRepository;
import com.blog.webblog.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public Category getCategoryByName(String name) {
        return categoryRepository.findByName(name);
    }

    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    public void deleteCategory(Long id) {
        categoryRepository.delete(categoryRepository.getById(id));
    }

    public void saveCategory(Category category) {
        categoryRepository.save(category);
    }
}
