package com.blog.webblog.service;

import com.blog.webblog.entity.User;
import com.blog.webblog.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public User getUserByEmailAndPassword(String email, String password) {
        return userRepository.findByEmailAndPassword(email, password);
    }

    public User getUser(Long id) {
        return userRepository.findById(id).orElse(null);
    }
}
