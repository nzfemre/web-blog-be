package com.blog.webblog.service;

import com.blog.webblog.entity.Comment;
import com.blog.webblog.entity.User;
import com.blog.webblog.repository.CommentRepository;
import com.blog.webblog.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;

    public void saveComment(Comment comment) {
        commentRepository.save(comment);
    }

    public List<Comment> getCommentsByPostId(Long id) {
        return commentRepository.findAllByPostId(id);
    }

    public void deleteComment(Long id) {
        commentRepository.delete(commentRepository.getById(id));
    }
}
