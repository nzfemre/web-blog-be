package com.blog.webblog.service;

import com.blog.webblog.entity.Param;
import com.blog.webblog.entity.Suggestion;
import com.blog.webblog.repository.ParamRepository;
import com.blog.webblog.repository.SuggestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ParamService {
    private final ParamRepository paramRepository;

    public List<Param> getParams() {
        return paramRepository.findAll();
    }
}
