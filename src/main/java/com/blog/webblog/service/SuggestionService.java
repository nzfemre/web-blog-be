package com.blog.webblog.service;

import com.blog.webblog.entity.Post;
import com.blog.webblog.entity.Suggestion;
import com.blog.webblog.repository.PostRepository;
import com.blog.webblog.repository.SuggestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SuggestionService {
    private final SuggestionRepository suggestionRepository;

    public List<Suggestion> getSuggestions() {
        return suggestionRepository.findAll();
    }

    public void saveSuggestion(Suggestion suggestion) {
        suggestionRepository.save(suggestion);
    }
}
