package com.blog.webblog.service;

import com.blog.webblog.entity.Post;
import com.blog.webblog.entity.User;
import com.blog.webblog.repository.PostRepository;
import com.blog.webblog.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PostService {
    private final PostRepository postRepository;

    public List<Post> getPosts() {
        return postRepository.findAll();
    }

    public Post getPost(Long id) {
        return postRepository.findById(id).get();
    }

    public void savePost(Post post) {
        postRepository.save(post);
    }

    public void deletePost(Long id) {
        postRepository.delete(postRepository.getById(id));
    }
}
