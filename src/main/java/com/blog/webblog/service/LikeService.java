package com.blog.webblog.service;

import com.blog.webblog.entity.Like;
import com.blog.webblog.entity.User;
import com.blog.webblog.repository.LikeRepository;
import com.blog.webblog.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LikeService {
    private final LikeRepository likeRepository;

    public void saveLike(Like like) {
        likeRepository.save(like);
    }
}
