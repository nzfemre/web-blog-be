package com.blog.webblog.dto;

import com.blog.webblog.LikeAction;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LikeDto {
    private Long postId;
    private Long userId;
    private LikeAction likeAction;
}
