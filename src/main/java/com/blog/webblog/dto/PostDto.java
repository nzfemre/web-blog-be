package com.blog.webblog.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostDto {
    private Long id;
    private String title;
    private String context;
    private String category;
    private Long userId;
}
