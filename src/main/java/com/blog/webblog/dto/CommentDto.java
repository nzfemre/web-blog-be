package com.blog.webblog.dto;

import com.blog.webblog.LikeAction;
import com.blog.webblog.entity.Post;
import com.blog.webblog.entity.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
public class CommentDto {
    private Long id;
    private String context;
    private Long userId;
    private Long postId;
}
